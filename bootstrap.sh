 
 wget https://releases.hashicorp.com/packer/1.5.6/packer_1.5.6_linux_amd64.zip
 unzip packer_1.5.6_linux_amd64.zip
 mv ./packer /usr/local/bin
 rm -rf packer_1.5.6_linux_amd64.zip packer

 wget  https://releases.hashicorp.com/terraform/0.12.25/terraform_0.12.25_linux_amd64.zip
 unzip terraform_0.12.25_linux_amd64.zip
 mv ./terraform /usr/local/bin
 rm -rf terraform_0.12.25_linux_amd64.zip terraform

 wget https://releases.hashicorp.com/vault/1.4.2/vault_1.4.2_linux_amd64.zip
 unzip vault_1.4.2_linux_amd64.zip
 mv ./vault /usr/local/bin
 rm -rf vault_1.4.2_linux_amd64.zip vault