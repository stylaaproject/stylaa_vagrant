#!/bin/sh

#echo "kill any krakend designer container"
#docker kill krakendesigner
kill $(sudo lsof -t -i:3000)
echo "launch grafana"
docker stop grafana
docker rm grafana
docker run -d --name=grafana -p 3000:3000 grafana/grafana-enterprise:8.2.5-ubuntu
