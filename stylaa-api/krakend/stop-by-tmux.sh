#!/bin/sh

tmux kill-session -t run_krakend
echo "shutdown  krakend ........."

tmux kill-session -t run_xmysql
echo "shutdown  xmysql ........."

tmux kill-session -t run_grafana
echo "shutdown  grafana ........."

tmux kill-session -t run_backendesign
echo "shutdown  backendesign ........."

tmux kill-session -t run_jaeger
echo "shutdown  jaeger ........."

echo "done!"

