#!/bin/sh

#echo "kill any krakend designer container"
#docker kill krakendesigner
kill $(sudo lsof -t -i:5775)
kill $(sudo lsof -t -i:6831)
kill $(sudo lsof -t -i:6832)
kill $(sudo lsof -t -i:5778)
kill $(sudo lsof -t -i:16686)
kill $(sudo lsof -t -i:14268)
kill $(sudo lsof -t -i:14250)
kill $(sudo lsof -t -i:9411)
echo "launch jaeger"
docker stop jaeger
docker rm jaeger
docker run -d --name jaeger \
  -e COLLECTOR_ZIPKIN_HOST_PORT=:9411 \
  -p 5775:5775/udp \
  -p 6831:6831/udp \
  -p 6832:6832/udp \
  -p 5778:5778 \
  -p 16686:16686 \
  -p 14268:14268 \
  -p 14250:14250 \
  -p 9411:9411 \
  jaegertracing/all-in-one:latest


