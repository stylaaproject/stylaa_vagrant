#!/bin/sh

tmux kill-session -t run_krakendesign
sleep 2
tmux new-session -ds run_krakendesign "bash --init-file krakendesign.sh" # starts new session named tasklist
echo "executing krakendesign ........."

echo "Restarting nginx ........."
sudo service nginx restart

echo "done!"

