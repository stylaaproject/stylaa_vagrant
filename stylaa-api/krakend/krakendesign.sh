#!/bin/sh

#echo "kill any krakend designer container"
#docker kill krakendesigner
kill $(sudo lsof -t -i:8080)
echo "launch krakend designer"
docker stop krakendesigner
docker rm krakendesigner
#docker run -d -p 8080:80 devopsfaith/krakendesigner --name=krakendesigner
#docker run -p 8080:80 -v $PWD:/etc/krakend/ devopsfaith/krakend run --config /etc/krakend/krakend.json

docker run --name krakendesigner -p 8080:80 -v $PWD:/home/vagrant/krakend/ devopsfaith/krakend run --config /home/vagrant/krakend/krakend-dev.json 


