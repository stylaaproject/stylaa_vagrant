#!/bin/sh

tmux kill-session -t run_xmysql
sleep 2
tmux new-session -d -s run_xmysql "bash --init-file xmysql.sh" # starts new session named tasklist
echo "executing xmysql ........."

#:wqtmux a
#tmux detach

tmux kill-session -t run_krakend
sleep 2
tmux new-session -ds run_krakend "bash --init-file krakend.sh" # starts new session named tasklist
echo "executing krakend ........."

# tmux a
# tmux detach
 tmux kill-session -t run_backendesign
 sleep 2
 tmux new-session -ds run_backendesign "bash --init-file krakendesign.sh" # starts new session named tasklist
 echo "executing krakendesign ........."

# tmux a
 tmux kill-session -t run_grafana
 sleep 2
 tmux new-session -ds run_grafana "bash --init-file grafana.sh" # starts new session named tasklist
 echo "executing grafana ........."

 tmux kill-session -t run_jaeger
 sleep 2
 tmux new-session -ds run_jaeger "bash --init-file jaeger.sh" # starts new session named tasklist
 echo "executing jaeger ........."


echo "Restarting nginx ........."
sudo service nginx restart

echo "done!"

