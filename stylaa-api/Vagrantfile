# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "ubuntu/bionic64"

  config.ssh.insert_key = false
  config.vm.boot_timeout = 800
  config.ssh.private_key_path = ["/Users/rockaja/.ssh/id_rsa", "~/.vagrant.d/insecure_private_key"]
  config.vm.provision "file", source: "/Users/rockaja/.ssh/id_rsa.pub", destination: "~/.ssh/authorized_keys"



  config.vm.hostname = "Stylaa"
  #config.vm.define "name2"
  config.vm.provider "virtualbox" do |p|
      p.name = "Stylaa-api-services"
  end

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port


  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
   config.vm.network "private_network", ip: "192.168.56.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  #config.vm.network "public_network", ip: "192.168.33.10"
  #config.vm.network "forwarded_port", guest: 80, host: 8080
  #config.vm.network "forwarded_port", guest: 9001, host: 9001, host_ip: "127.0.0.1"
  #config.vm.network "forwarded_port", guest: 9002, host: 9002, host_ip: "127.0.0.1"


  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.


  # Use NFS for the shared folder
    config.vm.synced_folder "/Users/rockaja/Documents/workspace/stylaa/git/stylaa_vagrant/stylaa-api/krakend", "/home/vagrant/krakend",
      id: "krakend" # <--- this ID must be unique
      #:nfs => true,
      #:mount_options => ['nolock,vers=3,udp,noatime']

  # Use NFS for the shared folder
    config.vm.synced_folder "/Users/rockaja/Documents/workspace/stylaa/git/stylaa_vagrant/stylaa-api/nginx", "/etc/nginx/sites-available",
      id: "nginxconfig" # <--- different from this one
      #:nfs => true,
      #:mount_options => ['nolock,vers=3,udp,noatime']

  # Use NFS for the shared folder
    config.vm.synced_folder "/Users/rockaja/Documents/workspace/stylaa/git/stylaa_vagrant/stylaa-api/xmysql", "/home/vagrant/xmysql",
      id: "xmysql" # <--- different from this one
      #:nfs => true,
      #:mount_options => ['nolock,vers=3,udp,noatime']

  # Use NFS for the shared folder
    config.vm.synced_folder "/Users/rockaja/Documents/workspace/stylaa/git/stylaa_vagrant/stylaa-api/schema", "/home/vagrant/schema",
      id: "schema" # <--- different from this one
      #:nfs => true,
      #:mount_options => ['nolock,vers=3,udp,noatime']

  # Use NFS for the shared folder
    config.vm.synced_folder "/Users/rockaja/Documents/workspace/stylaa/git/stylaa_vagrant/stylaa-api/installs", "/home/vagrant/installs",
      id: "installs" # <--- different from this one
      #:nfs => true,
      #:mount_options => ['nolock,vers=3,udp,noatime']

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
    config.vm.provision "shell", inline: <<-SHELL
      apt-get update -y
      apt-get install -y nginx 
      apt-get install -y curl 
      apt-get install -y wget 
      apt-get install -y tree 
      apt-get install -y zip 
      apt-get install -y unzip
      apt-get install -y git
      apt-get install gcc -y
      apt-get dist-upgrade -y
      apt-get update -y  
      apt-get install build-essential -y
      apt-get install mysql-server  -y
      apt-get install npm -y
      apt-get install tmux -y

      apt-key adv --keyserver keyserver.ubuntu.com --recv 5DE6FD698AD6FDD2
      echo "deb https://repo.krakend.io/apt stable main" | tee /etc/apt/sources.list.d/krakend.list
      apt-get update
      apt-get install -y krakend

    SHELL
    
    config.vm.provision :shell, path: "./bootstrap.sh"
end
