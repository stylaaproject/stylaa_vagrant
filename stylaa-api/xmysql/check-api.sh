#/bin/bash

#Get Status od service
#curl -s -o /dev/null -I -w "%{http_code}" localhost > status.txt
#STATUS=$(cat status.txt)
#check what port is used for - sudo lsof -i:9000
#kill pid on port 9000 - kill $(sudo lsof -t -i:9000)
DB=localhost
USER=vagrant
PASS=Kolawole55
LOG=/home/vagrant/xmysql/log/restarts

mysqladmin -h $DB -u$USER -p$PASS processlist

 if [ $? -eq 0 ]
    then
        echo "Connection to database is active " $(date)  >> $LOG
    else
        echo "Database connection error "  $(date) >> $LOG
        echo "Will not start API service until connection is resolved" $(date) >> $LOG
        echo "Connection alert has been sent to tech team on slack" $(date) >> $LOG
        exit 1
 fi

#Get Status od service
STATUS=$(curl -s -o /dev/null -I -w "%{http_code}" localhost:9001/api/users/1)

#Check variable
echo $STATUS


#Restart service if dead or hanged
if [ $STATUS -eq 200 ]
then
   echo "Status is $STATUS Nothing to do - all is good"
   echo "Status: $STATUS. Service is healthy" $(date)  >> $LOG
else
   echo "________________________________________________________________" >> $LOG
   echo ""
   echo "Status is $STATUS API is not responding " $(date)  >> $LOG
   echo "killing the service on port $PORT if gone awol!"  >> $LOG
   kill $(sudo lsof -t -i:9000) >> $LOG
   echo "Restarting xmysql api service on port $PORT $(date)"  >> $LOG
   START_SERVICE=$(sh /home/vagrant/xmysql/start-api.sh >> $LOG)
   echo "________________________________________________________________"  >> $LOG
fi

#echo "run this -- "
#whoami
#sh /home/vagrant/xmysql/start-api.sh