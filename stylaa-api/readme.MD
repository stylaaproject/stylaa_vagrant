README

This is the development environment for stylaa API Services

Services
API Service (xmysql)
A service that matches the business model in to a dynamic on demand api service

API-Gateway Service (krakend)
A service that sits as a gateway between API Service Layer (xmysql) and Access Control layer (keycloak)

Access Control Layer (keycloak) (Not added)
This service deals with authourisation and authentication.

Instructions
Run vagrant up

check if xmysql and krakend service is running
XMYSQL 9001 - example curl 192.168.33.10:9001/users/1
KRAKEND 9002 - example curl 192.168.33.10:9002/profile/1


Access flow
MYSQL DB -> API SERVICE -> API GATEWAY -> ACCESS CONTROL


config.vm.network "hostonly", "192.168.56.10"